import * as fs from "fs";
import { defineConfig } from "cypress";
import { ConfigData } from "html-validate";
import htmlvalidate from "cypress-html-validate/plugin";
import { formatter, CypressHtmlValidateOptions } from "cypress-html-validate";

const config: ConfigData = {
	rules: {
		"require-sri": "off",
		"script-type": "off",
	},
};

const options: Partial<CypressHtmlValidateOptions> = {
	include: ["html"],
	formatter(messages) {
		const stripped = messages.map((it) => {
			/* no persistent during builds */
			delete it.line;
			delete it.column;
			delete it.offset;
			return it;
		});
		const json = JSON.stringify(stripped, null, 2);
		fs.mkdirSync("temp", { recursive: true });
		fs.writeFileSync("temp/cypress-result.json", json, "utf-8");
		formatter(messages);
	},
};

module.exports = defineConfig({
	screenshotOnRunFailure: false,
	video: false,
	e2e: {
		setupNodeEvents(on) {
			htmlvalidate.install(on, config, options);
		},
		baseUrl: "http://127.0.0.1:8080",
	},
});
