import "cypress-html-validate/commands";

/* automatically run html-validate after each test */
afterEach(() => {
	cy.htmlvalidate();
});
