describe("search function", () => {
	beforeEach(() => {
		cy.visit("/");
	});

	it("should show results", () => {
		cy.get("#results a").should("have.length", 0);
		cy.get("#search").type("tel");
		cy.get("#results a").should("have.length.greaterThan", 0);
		cy.get("#results a").first().click();
		cy.get("h1").should("contain.text", "Product: ");
	});
});
