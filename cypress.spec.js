const fs = require("fs");
const path = require("path");

const resultFilename = path.join(__dirname, "temp/cypress-result.json");

expect.extend({
	/**
	 * @this {import("jest").MatcherContext)
	 * @param {any} received
	 * @returns {import("jest").CustomMatcherResult}
	 */
	toExist(received) {
		const exists = fs.existsSync(received);
		return {
			pass: exists,
			message() {
				if (exists) {
					return `"${received}" exists when it shouldn't`;
				} else {
					return `"${received}" does not exists when it should`;
				}
			},
		};
	},
});

it("should contain errors", () => {
	expect.assertions(2);
	expect(resultFilename).toExist();
	const results = JSON.parse(fs.readFileSync(resultFilename, "utf-8"));
	expect(results).toMatchInlineSnapshot(`
		[
		  {
		    "context": {
		      "attribute": "lang",
		      "element": "html",
		    },
		    "message": "<html> is missing required "lang" attribute",
		    "ruleId": "element-required-attributes",
		    "ruleUrl": "https://html-validate.org/rules/element-required-attributes.html",
		    "selector": "html",
		    "severity": 2,
		    "size": 4,
		  },
		  {
		    "message": "Anchor link must have a text describing its purpose",
		    "ruleId": "wcag/h30",
		    "ruleUrl": "https://html-validate.org/rules/wcag/h30.html",
		    "selector": "#logo > div > a:nth-child(1)",
		    "severity": 2,
		    "size": 1,
		  },
		  {
		    "message": "<img> is missing required "alt" attribute",
		    "ruleId": "wcag/h37",
		    "ruleUrl": "https://html-validate.org/rules/wcag/h37.html",
		    "selector": "#logo > div > a:nth-child(1) > img",
		    "severity": 2,
		    "size": 3,
		  },
		  {
		    "message": "Initial heading level must be <h1> but got <h2>",
		    "ruleId": "heading-level",
		    "ruleUrl": "https://html-validate.org/rules/heading-level.html",
		    "selector": "#logo > div > a:nth-child(2) > h2",
		    "severity": 2,
		    "size": 2,
		  },
		  {
		    "context": {
		      "replacement": "main",
		      "role": "main",
		    },
		    "message": "Prefer to use the native <main> element",
		    "ruleId": "prefer-native-element",
		    "ruleUrl": "https://html-validate.org/rules/prefer-native-element.html",
		    "selector": "#main",
		    "severity": 2,
		    "size": 11,
		  },
		  {
		    "message": "Heading level can only increase by one, expected <h2> but got <h5>",
		    "ruleId": "heading-level",
		    "ruleUrl": "https://html-validate.org/rules/heading-level.html",
		    "selector": "#footer > div > div:nth-child(1) > h5",
		    "severity": 2,
		    "size": 2,
		  },
		  {
		    "message": "<form> element must have a submit button",
		    "ruleId": "wcag/h32",
		    "ruleUrl": "https://html-validate.org/rules/wcag/h32.html",
		    "selector": "#searchbox > form",
		    "severity": 2,
		    "size": 4,
		  },
		  {
		    "message": "<input> element does not have a <label>",
		    "ruleId": "input-missing-label",
		    "ruleUrl": "https://html-validate.org/rules/input-missing-label.html",
		    "selector": "#search",
		    "severity": 2,
		    "size": 5,
		  },
		  {
		    "context": {
		      "replacement": "button",
		      "role": "button",
		    },
		    "message": "Prefer to use the native <button> element",
		    "ruleId": "prefer-native-element",
		    "ruleUrl": "https://html-validate.org/rules/prefer-native-element.html",
		    "selector": "#searchbox > form > div > p > span",
		    "severity": 2,
		    "size": 13,
		  },
		]
	`);
});
